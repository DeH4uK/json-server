var jsonServer = require('json-server')
var server = jsonServer.create()
var router = jsonServer.router('db.json')
var middlewares = jsonServer.defaults()

server.use(middlewares)

/*server.post('/orders', function (req, res) {
  var db = router.db // lowdb instance
  var tasks = {test: 'test'}

  res.jsonp(tasks)
})

server.get('/orders', function (req, res) {
  var db = router.db // lowdb instance
  var tasks = {test: 'testget'}
  console.log(db.orders)
  res.jsonp(db)
})*/

const putRegistrationDataTask = /task=putRegistrationData/i
const getUserDataTask = /task=getUserData/i
const toAuthorizeUserTask = /task=toAuthorizeUser/i
const getOrdersListTask = /task=getOrdersList/i
const getOrderInfoTask = /task=getOrderInfo/i

router.render = (req, res) => {
  resLocalsData = JSON.parse(JSON.stringify(res.locals.data))
  if (req.method == 'POST') {

    if (putRegistrationDataTask.test(req.url)) {
      delete resLocalsData.password
      delete resLocalsData.email
      delete resLocalsData.username
      delete resLocalsData.phone
      resLocalsData.userId = resLocalsData.id
      resLocalsData.token = 'test_token'
      delete resLocalsData.id
    }
    
  }
  if (req.method == 'GET') {

    if (getUserDataTask.test(req.url)) {
      delete resLocalsData.password
      resLocalsData.ordersCount = 999
    }

    if (toAuthorizeUserTask.test(req.url)) {
      if (resLocalsData.length !== 0) {
        resLocalsData = resLocalsData[0];
        delete resLocalsData.password
        delete resLocalsData.email
        delete resLocalsData.username
        delete resLocalsData.phone
        resLocalsData.userId = resLocalsData.id
        resLocalsData.token = 'test_token'
        delete resLocalsData.id
      } else {
        resLocalsData = null;
      }
    }

  }
  res.jsonp(resLocalsData)
}

server.use(jsonServer.rewriter({
  '/getUserData?id=:id&token=:token': '/users/:id?task=getUserData',
  '/putRegistrationData': '/users?task=putRegistrationData',
  '/toAuthorizeUser?username=:username&password=:password': '/users?username=:username&password=:password&task=toAuthorizeUser',
  '/getOrdersList?userId=:userId&token=:token': '/orders?userId=:userId&task=getOrdersList',
  '/getOrderInfo?id=:id': '/orders/:id?task=getOrderInfo'
}));
server.use(router)
server.listen(3000, function () {
  console.log('JSON Server is running')
})